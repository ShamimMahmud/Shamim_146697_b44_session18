<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 10:20 AM
 */

namespace App;

use App\Database;


class Result extends Database
{
    private $id;
    private $name;
    private $roll;
    private $markBangla;
    private $markEnglish;
    private $markMath;

    private $gradeBangla;
    private $gradeEnglish;
    private $gradeMath;

    public function setData($postData){

        if(array_key_exists("id",$postData)){

            $this->id=$postData['id'];
        }

        if(array_key_exists("name",$postData)){

            $this->name=$postData['name'];
        }

        if(array_key_exists("roll",$postData)){

            $this->roll=$postData['roll'];
        }

        if(array_key_exists("markbangla",$postData)){

            $this->markBangla=$postData['markbangla'];
        }

        if(array_key_exists("markenglish",$postData)){

            $this->markEnglish=$postData['markenglish'];
        }
        if(array_key_exists("markmath",$postData)){

            $this->markMath=$postData['markmath'];
        }
        if(array_key_exists("markbangla",$postData)){


            $this->gradeBangla=$this->convertMark2Grade($this->markBangla);
        }

        if(array_key_exists("markenglish",$postData)){

            $this->gradeEnglish=$this->convertMark2Grade($this->markEnglish);
        }
        if(array_key_exists("grademath",$postData)){

            $this->gradeMath=$this->convertMark2Grade($this->markMath);
        }


    }
public function convertMark2Grade($mark){

    switch($mark){

        case ($mark>79):
            return "A+";
        case ($mark>74):
            return "A";
        case ($mark>69):
            return "A-";
        case ($mark>64):
            return "B";
        case ($mark>59):
            return "B-";
        case ($mark>49):
            return "C+";
        case ($mark>44):
            return "C";
        case ($mark>40):
            return "C";
        default:
            return "F";




    }




}



    public function store($arrData){

        $arrData=array($this->name,$this->roll,$this->markBangla,$this->markEnglish,$this->markMath,$this->gradeBangla,$this->gradeEnglish,$this->gradeMath);

        $sql="insert into result(name,roll,mark_bangla,mark_english,mark_math,grade_bangla,grade_english,grade_math) values(?,?,?,?,?,?,?,?) ";

        $STH=$this->DBH->prepare($sql);

        $success=$STH->execute($arrData);

        if($success){

            Message::message("Successful! Data has been successfully inserted");
            echo Message::message();
        }
        else{
            Message::message("Failed to insert Data");

        }

        Utility::redirect("information_collection.php");
    }

}